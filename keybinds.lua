local awful     = require("awful")
local beautiful = require("beautiful")
local lain      = require("lain")

local gui_editor       = "emacsclient -c -a emacs"
local browser          = "firefox"
local email            = "evolution"
local file_manager     = "spacefm"
local fast_browser     = "surf -g duckduckgo.org"
local buffers_p2c      = "xclip -o -selection primary | xclip -i -selection clipboard"
local buffers_c2p      = "xclip -o -selection clipboard | xclip -i -selection primary"
local slugify          = [[slugify --separator _ --no-lowercase -- $(xclip -o -selection clipboard) | sed -z 's/\n//g' | xclip -i -selection clipboard]]
local unnewline        = [[xclip -o -selection clipboard | sed -z 's/\n//g' | xclip -i -selection clipboard]]
local calendar         = "gnome-calendar"
local passmanager      = "keepassxc"
local screenshot       = "flameshot gui --delay 3000"
local volume           = "pavucontrol"


local function pa_with_default_sink(cmd)
   awful.spawn.easy_async_with_shell(
      [[pactl --format=json info | jq -r '.default_sink_name']],
      function(default_sink_name)
         if default_sink_name then
            awful.spawn.easy_async_with_shell(
               string.format(cmd, default_sink_name:gsub("%s+", "")),
               function()
                  beautiful.volume.update()
               end
            )
         end
      end
   )
end

local prgmap = {
   { "q", function() awful.spawn(browser) end,                   "Browser" },
   { "w", function() awful.spawn(fast_browser) end,              "Fast Browser" },
   { "f", function() awful.spawn(file_manager) end,              "File Manager" },
   { "e", function() awful.spawn(email) end,                     "Email Client" },
   { "p", function() awful.util.spawn(screenshot) end,           "Screenshot" },
   { "s", function() awful.spawn(gui_editor) end,                "Editor" },
   { "c", function() awful.spawn(calendar) end,                  "Calendar" },
   { "k", function() awful.spawn(passmanager) end,               "Pswd Manager" },
   { "v", function() awful.spawn(volume) end,                    "Volume" },
}

local mediamap = {
   { "separator", "MPD" },
   { "s", function() awful.util.spawn("mpd") end,        "start MPD" },
   { "k", function() awful.util.spawn("mpd --kill") end, "kill MPD" },
   { "g", function() awful.util.spawn("gmpc") end,       "GMPC" },
   { "separator", "Volume" },
   { "Up", function() pa_with_default_sink("pactl set-sink-volume %s +5%%") end, "Vol up" },
   { "Down", function() pa_with_default_sink("pactl set-sink-volume %s -5%%") end, "Vol down" },
   { "=", function() pa_with_default_sink("pactl set-sink-volume %s +1%%") end, "Vol up 1%" },
   { "-", function() pa_with_default_sink("pactl set-sink-volume %s -1%%") end, "Vol down 1%" },
   { " ", function() pa_with_default_sink("pactl set-sink-mute %s toggle") end, "Mute" },
   { "0", function() pa_with_default_sink("pactl set-sink-volume %s 0%%") end, "Zero" },
   { "separator", "Playback" },
   { "Return",
     function()
        awful.util.spawn("mpc toggle -h " .. beautiful.mpd.host .. " -p " .. beautiful.mpd.port)
        beautiful.mpd.update()
     end,
     "Toggle" },
   { "Delete",
     function()
        awful.util.spawn("mpc stop -h " .. beautiful.mpd.host .. " -p " .. beautiful.mpd.port)
        beautiful.mpd.update()
     end,
     "Stop" },
   { "Right",
     function()
        awful.util.spawn("mpc next -h " .. beautiful.mpd.host .. " -p " .. beautiful.mpd.port)
        beautiful.mpd.update()
     end,
     "Next" },
   { "Left",
     function()
        awful.util.spawn("mpc prev -h " .. beautiful.mpd.host .. " -p " .. beautiful.mpd.port)
        beautiful.mpd.update()
     end,
     "Prev" },
}


local windowmap = {
   { "]", function () awful.tag.incmwfact(0.05) end,           "Inc Factor"},
   { "'", function () awful.tag.incmwfact(-0.05) end,          "Dec Factor"},
   { "-",
     function ()
        for s in screen do
           for _, t in ipairs(s.tags) do
              lain.util.useless_gaps_resize(1, nil, t)
           end
        end
     end,
     "Inc Gap"},
   { "=",
     function ()
        for s in screen do
           for _, t in ipairs(s.tags) do
              lain.util.useless_gaps_resize(-1, nil, t)
           end
        end
     end,
     "Dec Gap"},
   { "l", function () awful.tag.incnmaster(1, nil, true) end,  "Inc Master"},
   { "k", function () awful.tag.incnmaster(-1, nil, true) end, "Dec Master"},
   { ";", function () awful.tag.incncol(1, nil, true) end,     "Inc Columns"},
   { "j", function () awful.tag.incncol(-1, nil, true) end,    "Dec Columns"},
   { "o", function () awful.client.focus.byidx(1) end,         "Next Client"},
   { "i", function () awful.client.focus.byidx(-1) end,        "Prev Client"},
   { "p", function () awful.client.swap.byidx(1) end,          "Next Swap"},
   { "u", function () awful.client.swap.byidx(-1) end,         "Prev Swap"},
   { "x", function () awful.layout.inc(1) end,                 "Next Layout"},
   { "z", function () awful.layout.inc(-1) end,                "Prev Layout"},
}

local tagsmap = {
   { "n", lain.util.add_tag,           "New" },
   { "r", lain.util.rename_tag,        "Rename" },
   { "d", lain.util.delete_tag,        "Delete" },
}

local awesomemap = {
   { "r", awesome.restart,            "Restart" },
   { "q", awesome.quit,               "Quit" },
   { "u", awful.client.urgent.jumpto, "Urgent" },
   { "c",
     function()
	for s in screen do
	   awful.tag.viewnone(s)
	end
     end
     , "Hide tags" },
   { "l", function() awful.util.spawn({"dm-tool", "lock"}, false) end, "Lock" },
   { "k", function() awful.util.spawn({"xset", "dpms", "force", "off"}, false) end, "DPMS" },
   { "n",
     function ()
        local c = awful.client.restore()
        -- Focus restored client
        if c then
           client.focus = c
           c:raise()
        end
     end,
     "Restore client" },
   { "x",
     function ()
        awful.prompt.run {
           prompt       = "Run Lua code: ",
           textbox      = awful.screen.focused().mypromptbox.widget,
           exe_callback = awful.util.eval,
           history_path = awful.util.get_cache_dir() .. "/history_eval"
        }
     end,
     "Lua" },
   { "b",
     function ()
        for s in screen do
           s.mywibox.visible = not s.mywibox.visible
           if s.mybottomwibox then
              s.mybottomwibox.visible = not s.mybottomwibox.visible
           end
        end
     end,
     "WiBar" },
   { "Right", function() awful.spawn.with_shell(buffers_p2c) end,    "Prim→Clip" },
   { "Left", function() awful.spawn.with_shell(buffers_c2p) end,    "Prim←Clip" },
}

local clientmap = {
   { "o", lain.util.magnify_client, "Magnify" },
   { "b",
     function(c)
        c.fullscreen = not c.fullscreen
        c:raise()
     end,
     "Fullscreen" },
   { "f", awful.client.floating.toggle, "✈Float" },
   { "r", function (c) c:swap(awful.client.getmaster()) end, "Move to master" },
   { "s", function (c) c:move_to_screen() end, "Move to screen" },
   { "t", function (c) c.ontop = not c.ontop end, "⌃Keep on top" },
   { "p", function (c) c.above = not c.above end, "▴Above" },
   { "i", function (c) c.below = not c.below end, "▾Below" },
   { "y", function (c) c.sticky = not c.sticky end, "▪Sticky" },
   { "g", function (c) c.maximized_horizontal = not c.maximized_horizontal end,
     "⬌Maximized horizontal" },
   { "h", function (c) c.maximized_vertical = not c.maximized_vertical end,
     "⬍Maximized vertical" },
   { "n",
     function (c)
         -- The client currently has the input focus, so it cannot be
         -- minimized, since minimized clients can't have the focus.
         c.minimized = true
      end ,
     "Minimize" },
   { "m",
     function (c)
        c.maximized = not c.maximized
        c:raise()
     end ,
     "+Maximize" },
}

local shortmap = {
   { "s", function() awful.spawn.with_shell(slugify) end, "Slugify" },
   { "n", function() awful.spawn.with_shell(unnewline) end, "Unnewline" },
}


return {prgmap=prgmap,
        mediamap=mediamap,
        tagsmap=tagsmap,
        windowmap=windowmap,
        awesomemap=awesomemap,
        clientmap=clientmap,
        shortmap=shortmap,
        pa_with_default_sink=pa_with_default_sink}
