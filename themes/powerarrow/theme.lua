
--[[
   Powerarrow Awesome WM theme
   github.com/copycat-killer
--]]

local gears      = require("gears")
local lain       = require("lain")
local awful      = require("awful")
local wibox      = require("wibox")
local naughty    = require("naughty")
local mailboxes  = require("mailboxes")
local os, math, string = os, math, string


local theme                                     = {}
theme.dir                                       = os.getenv("HOME") .. "/.config/awesome/themes/powerarrow"
theme.wallpaper                                 = theme.dir .. "/wall.png"
theme.font                                      = "xos4 Terminus 9"
theme.fg_normal                                 = "#FEFEFE"
theme.fg_focus                                  = "#32D6FF"
theme.fg_urgent                                 = "#C83F11"
theme.bg_normal                                 = "#222222"
theme.bg_focus                                  = "#1E2320"
theme.bg_urgent                                 = "#3F3F3F"
theme.taglist_fg_focus                          = "#00CCFF"
theme.tasklist_bg_focus                         = "#222222"
theme.tasklist_fg_focus                         = "#00CCFF"
theme.border_width                              = 1
theme.border_normal                             = "#3F3F3F"
theme.border_focus                              = "#00CCFF"
theme.border_marked                             = "#CC9393"
theme.titlebar_bg_focus                         = "#3F3F3F"
theme.titlebar_bg_normal                        = "#3F3F3F"
theme.titlebar_bg_focus                         = theme.bg_focus
theme.titlebar_bg_normal                        = theme.bg_normal
theme.titlebar_fg_focus                         = theme.fg_focus
theme.modebox_fg                                = theme.fg_focus
theme.modebox_bg                                = theme.bg_focus
theme.modebox_border                            = theme.border_focus
theme.modebox_border_width                      = theme.border_width
theme.menu_height                               = 16
theme.menu_width                                = 140
theme.terminal                                  = theme.dir .. "/icons/terminal.ico"
theme.menu_submenu_icon                         = theme.dir .. "/icons/submenu.png"
theme.awesome_icon                              = theme.dir .. "/icons/awesome.png"
theme.taglist_squares_sel                       = theme.dir .. "/icons/square_sel.png"
theme.taglist_squares_unsel                     = theme.dir .. "/icons/square_unsel.png"
theme.layout_tile                               = theme.dir .. "/icons/tile.png"
theme.layout_tileleft                           = theme.dir .. "/icons/tileleft.png"
theme.layout_tilebottom                         = theme.dir .. "/icons/tilebottom.png"
theme.layout_tiletop                            = theme.dir .. "/icons/tiletop.png"
theme.layout_fairv                              = theme.dir .. "/icons/fairv.png"
theme.layout_fairh                              = theme.dir .. "/icons/fairh.png"
theme.layout_spiral                             = theme.dir .. "/icons/spiral.png"
theme.layout_dwindle                            = theme.dir .. "/icons/dwindle.png"
theme.layout_max                                = theme.dir .. "/icons/max.png"
theme.layout_fullscreen                         = theme.dir .. "/icons/fullscreen.png"
theme.layout_magnifier                          = theme.dir .. "/icons/magnifier.png"
theme.layout_floating                           = theme.dir .. "/icons/floating.png"
theme.widget_ac                                 = theme.dir .. "/icons/ac.png"
theme.widget_battery                            = theme.dir .. "/icons/battery.png"
theme.widget_battery_low                        = theme.dir .. "/icons/battery_low.png"
theme.widget_battery_empty                      = theme.dir .. "/icons/battery_empty.png"
theme.widget_mem                                = theme.dir .. "/icons/mem.png"
theme.widget_cpu                                = theme.dir .. "/icons/cpu.png"
theme.widget_temp                               = theme.dir .. "/icons/temp.png"
theme.widget_net                                = theme.dir .. "/icons/net.png"
theme.widget_hdd                                = theme.dir .. "/icons/hdd.png"
theme.widget_music                              = theme.dir .. "/icons/note.png"
theme.widget_music_on                           = theme.dir .. "/icons/note_on.png"
theme.widget_music_pause                        = theme.dir .. "/icons/pause.png"
theme.widget_music_stop                         = theme.dir .. "/icons/stop.png"
theme.widget_vol                                = theme.dir .. "/icons/vol.png"
theme.widget_vol_low                            = theme.dir .. "/icons/vol_low.png"
theme.widget_vol_no                             = theme.dir .. "/icons/vol_no.png"
theme.widget_vol_mute                           = theme.dir .. "/icons/vol_mute.png"
theme.widget_mail                               = theme.dir .. "/icons/mail.png"
theme.widget_mail_on                            = theme.dir .. "/icons/mail_on.png"
theme.widget_task                               = theme.dir .. "/icons/task.png"
theme.widget_scissors                           = theme.dir .. "/icons/scissors.png"
theme.widget_poweroff                           = theme.dir .. "/icons/pow.png"
theme.widget_debian                             = theme.dir .. "/icons/debian.png"
theme.tasklist_plain_task_name                  = false
theme.tasklist_disable_icon                     = false
theme.useless_gap                               = 0
theme.titlebar_close_button_focus               = theme.dir .. "/icons/titlebar/close_focus.png"
theme.titlebar_close_button_normal              = theme.dir .. "/icons/titlebar/close_normal.png"
theme.titlebar_ontop_button_focus_active        = theme.dir .. "/icons/titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active       = theme.dir .. "/icons/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive      = theme.dir .. "/icons/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive     = theme.dir .. "/icons/titlebar/ontop_normal_inactive.png"
theme.titlebar_sticky_button_focus_active       = theme.dir .. "/icons/titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active      = theme.dir .. "/icons/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive     = theme.dir .. "/icons/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive    = theme.dir .. "/icons/titlebar/sticky_normal_inactive.png"
theme.titlebar_floating_button_focus_active     = theme.dir .. "/icons/titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active    = theme.dir .. "/icons/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive   = theme.dir .. "/icons/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive  = theme.dir .. "/icons/titlebar/floating_normal_inactive.png"
theme.titlebar_maximized_button_focus_active    = theme.dir .. "/icons/titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active   = theme.dir .. "/icons/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = theme.dir .. "/icons/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = theme.dir .. "/icons/titlebar/maximized_normal_inactive.png"
theme.icon_theme                                = "oxygen"
theme.monospaced_font                           = "Monospace 9"

local markup = lain.util.markup
local separators = lain.util.separators

--[[ Binary clock
   local binclock = require("themes.powerarrow.binclock"){
   height = 16,
   show_seconds = true,
   color_active = theme.fg_normal,
   color_inactive = theme.bg_focus
   }
--]]

local keyboardlayout = awful.widget.keyboardlayout()

-- Textclock
local textclock = wibox.widget.textclock(" %a %Y-%m-%d %X ", 1)
textclock.font = theme.font
theme.textclock = textclock

theme.tray = wibox.widget.systray()

-- Infobox

local infobox = wibox({
      ontop=true,
      visible=false,
      x=0,
      y=0,
      width=1,
      height=1,
      bg="#0005",
      shape=gears.shape.round_rect,
      type=dropdown_menu
})

infobox:setup({
      id="scroll",
      {
         {
            id="text",
            align="left",
            font=theme.monospaced_font,
            widget=wibox.widget.textbox
         },
         id="margin",
         left   = 5,
         right  = 5,
         top    = 5,
         bottom = 5,
         layout=wibox.container.margin
      },
      step_function = wibox.container.scroll.step_functions
         .waiting_nonlinear_back_and_forth,
      speed = 100,
      layout=wibox.container.scroll.vertical
})
infobox:get_children_by_id("scroll")[1]:pause()

infobox.widget:connect_signal(
   "button::press",
   function()
      infobox.visible=false
      infobox:get_children_by_id("scroll")[1]:pause()
      infobox:get_children_by_id("text")[1].text = ""
   end
)

function infobox:show_text(text, args)
   while text:sub(text:len()) == "\n" do
      text = text:sub(1, text:len()-1)
   end
   self.bg = args.bg or "#000000"
   local scr = awful.screen.focused()
   self:get_children_by_id("text")[1].align = args.align or "left"
   self:get_children_by_id("text")[1].text = text
   local x, y = infobox:get_children_by_id("text")[1]:
      get_preferred_size(scr)
   if y > scr.geometry.height then
      infobox:get_children_by_id("scroll")[1]:reset_scrolling()
      infobox:get_children_by_id("scroll")[1]:continue()
   end
   x = math.min(x + 10, scr.geometry.width)
   y = math.min(y + 10, scr.geometry.height)
   self.screen=scr
   self.width=x
   self.height=y
   awful.placement.top_right(self, {honor_padding=true,
                                    honor_workarea=true})
   infobox.visible=true
end

--[[ Calendar
   theme.cal = lain.widget.calendar({
   cal = '/usr/bin/ncal -bMh',
   attach_to = { textclock },
   followtag = true,
   notification_preset = {
   font = theme.monospaced_font,
   fg   = theme.fg_normal,
   bg   = theme.bg_normal
   }
   })
--]]

--[[ Taskwarrior
   local task = wibox.widget.imagebox(theme.widget_task)
   lain.widget.contrib.task.attach(task, {
   do not colorize output
   show_cmd = "task | sed -r 's/\\x1B\\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g'"
   })
   task:buttons(awful.util.table.join(awful.button({}, 1, lain.widget.contrib.task.prompt)))

   Scissors (xsel copy and paste)
   local scissors = wibox.widget.imagebox(theme.widget_scissors)
   scissors:buttons(awful.util.table.join(awful.button({}, 1, function() awful.spawn("xsel | xsel -i -b") end)))
--]]

-- Mail IMAP check
local mailicon = wibox.widget.imagebox(theme.widget_mail)

local mailcount1, mailcount2, mailcount3, mailcount4 = 0, 0, 0, 0
local mail, mail2, mail3, mail4 = nil, nil, nil, nil

mail = lain.widget.imap({
      timeout  = 60,
      plain = false,
      followtag = true,
      server   = mailboxes[1].server,
      mail     = mailboxes[1].mail,
      password = mailboxes[1].password,
      settings = function()
         mail_notification_preset.position = "top_right"
         mailcount1 = mailcount
         local mailtotal = mailcount1 + mailcount2 + mailcount3 + mailcount4
         if mailtotal > 0 then
            widget:set_text(" " .. mailtotal .. " ")
            mailicon:set_image(theme.widget_mail_on)
         else
            widget:set_text("")
            mailicon:set_image(theme.widget_mail)
         end
	 mail2.timer:again()
	 mail3.timer:again()
	 mail4.timer:again()
      end,
})

mail2 = lain.widget.imap({
      timeout  = 55,
      plain = false,
      followtag = true,
      server   = mailboxes[2].server,
      mail     = mailboxes[2].mail,
      password = mailboxes[2].password,
      settings = function()
         mail_notification_preset.position = "top_right"
         mailcount2 = mailcount
         local mailtotal = mailcount1 + mailcount2 + mailcount3 + mailcount4
         if mailtotal > 0 then
            mail.widget:set_text(" " .. mailtotal .. " ")
            mailicon:set_image(theme.widget_mail_on)
         else
            mail.widget:set_text("")
            mailicon:set_image(theme.widget_mail)
         end
      end,
})

mail3 = lain.widget.imap({
      timeout  = 50,
      plain = false,
      followtag = true,
      server   = mailboxes[3].server,
      mail     = mailboxes[3].mail,
      password = mailboxes[3].password,
      settings = function()
         mail_notification_preset.position = "top_right"
         mailcount3 = mailcount
         local mailtotal = mailcount1 + mailcount2 + mailcount3 + mailcount4
         if mailtotal > 0 then
            mail.widget:set_text(" " .. mailtotal .. " ")
            mailicon:set_image(theme.widget_mail_on)
         else
            mail.widget:set_text("")
            mailicon:set_image(theme.widget_mail)
         end
      end,
})

mail4 = lain.widget.imap({
      timeout  = 45,
      plain = false,
      followtag = true,
      server   = mailboxes[4].server,
      mail     = mailboxes[4].mail,
      password = mailboxes[4].password,
      settings = function()
         mail_notification_preset.position = "top_right"
         mailcount4 = mailcount
         local mailtotal = mailcount1 + mailcount2 + mailcount3 + mailcount4
         if mailtotal > 0 then
            mail.widget:set_text(" " .. mailtotal .. " ")
            mailicon:set_image(theme.widget_mail_on)
         else
            mail.widget:set_text("")
            mailicon:set_image(theme.widget_mail)
         end
      end,
})

mailicon:connect_signal(
   "button::press",
   function()
      mail2.update()
      mail3.update()
      mail4.update()
      mail.update()
   end
)

--[[ ALSA volume
   theme.volume = lain.widget.alsabar({
   --togglechannel = "IEC958,3",
   notification_preset = { font = "xos4 Terminus 10", fg = theme.fg_normal },
   })

   local volicon = wibox.widget.imagebox(theme.widget_vol)
   theme.volume = lain.widget.pulsebar({
   sink = 0,
   ticks = true,
   ticks_size = 7,
   notification_preset = { font = "xos4 Terminus 10", fg = theme.fg_normal },
   width = 40, height = 60, border_width = 0,
   colors = {
   background = theme.bg_normal,
   unmute     = theme.fg_normal,
   mute       = theme.fg_focus
   }
   })
   theme.volume.bar.margins = 4
   theme.volume.tooltip.wibox.fg = theme.fg_normal
   theme.volume.tooltip.wibox.bg = theme.bg_normal
--]]

local volicon = wibox.widget.imagebox(theme.widget_vol)
theme.volume = lain.widget.pulse({
      settings = function()
         local with_default_sink = function(cmd)
            awful.spawn.easy_async_with_shell(
               [[pactl --format=json info | jq -r '.default_sink_name']],
               function(name)
                  if name then
                     cmd(name:gsub("%s+", ""))
                  end
               end
            )
         end
         local with_sink_info = function(sink)
            awful.spawn.easy_async_with_shell(
               string.format([[pactl --format=json list sinks ]] ..
                  [=[| jq 'map(select(.name=="%s"))[]]=] ..
                  [[|{"name": .name, "index": .index, ]] ..
                  [["muted": .mute, "volume": .volume]] ..
                  [[|to_entries[0].value.value_percent}' ]] ..
                  [[| sed 's/[", ]//g']], sink),
               function(info)
                  if info then
                     index  = string.match(info, "index:(%S+)") or "N/A"
                     device = string.match(info, "name:(%S+)") or "N/A"
                     muted  = string.match(info, "muted:(%S+)") or "N/A"
                     volume  = string.match(info, "volume:(%S+)%%") or "N/A"
                     local infoline = volume .. "%"
                     if muted == "true" then
                        infoline = infoline .. " M"
                     end
                     theme.volume.widget:set_markup(markup.font(theme.font, infoline))
                  end
               end
            )
         end
         with_default_sink(with_sink_info)
      end,
      cmd = function() return "" end
})

-- MPD
local mpd_host = "0.0.0.0"
local mpd_port = "6600"

-- mdp song info crop contants
local max_len_artist = 10
local max_len_title = 15

local musicplr = awful.util.terminal .. " -title Music -g 130x34-320+16 -e ncmpcpp"
local mpdicon = wibox.widget.imagebox(theme.widget_music)
theme.mpd = lain.widget.mpd({
      host = mpd_host,
      port = mpd_port,
      settings = function()
         if mpd_now.state == "play" then
            if utf8.len(mpd_now.artist) > max_len_artist then
               artist = " " .. string.sub(mpd_now.artist, 1, utf8.offset(mpd_now.artist, max_len_artist)+1) .. ".. "
            else
               artist = " " .. mpd_now.artist .. " "
            end

            if utf8.len(mpd_now.title) > max_len_title then
               title  = " " .. string.sub(mpd_now.title, 1, utf8.offset(mpd_now.title, max_len_title)+1) .. ".. "
            else
               title = " " .. mpd_now.title .. " "
            end

            mpdicon:set_image(theme.widget_music_on)
            widget:set_markup(markup.font(theme.font, markup("#FF8466", artist) .. " " .. title))
         elseif mpd_now.state == "pause" then
            widget:set_markup(markup.font(theme.font, " mpd paused "))
            mpdicon:set_image(theme.widget_music_pause)
         else
            widget:set_text("")
            mpdicon:set_image(theme.widget_music)
         end
      end
})
theme.mpd.host = mpd_host
theme.mpd.port = mpd_port

-- Packages

local pkg_icon = wibox.widget.imagebox(theme.widget_debian)
local pkg_arr = wibox.container.background(wibox.container.margin(wibox.widget { pkg_icon, layout = wibox.layout.align.horizontal }, 3, 3), "#3f4251")
pkg_arr:connect_signal(
   "button::press",
   function()
      awful.spawn.easy_async("apt list --upgradable",
                             function(stdout, stderr, reason, exit_code)
                                infobox:show_text(stdout, {bg="#3f4251"})
                             end
      )
   end
)

-- MEM
local memicon = wibox.widget.imagebox(theme.widget_mem)
local mem = lain.widget.mem({
      settings = function()
         widget:set_markup(markup.font(theme.font, " " .. mem_now.used .. "MB "))
      end
})

local mem_arr = wibox.container.background(wibox.container.margin(wibox.widget { memicon, mem.widget, layout = wibox.layout.align.horizontal }, 2, 3), "#777E76")
mem_arr:connect_signal(
   "button::press",
   function()
      awful.spawn.easy_async(
         "ps -a -u " .. os.getenv("USER") .. " -o comm,pid,%cpu,%mem --sort=-%mem",
         function(stdout, stderr, reason, exit_code)
            infobox:show_text(stdout, {bg="#777E76"})
         end
      )
end)

-- CPU
local cpuicon = wibox.widget.imagebox(theme.widget_cpu)
local cpu = lain.widget.cpu({
      settings = function()
         widget:set_markup(markup.font(theme.font, " " .. cpu_now.usage .. "% "))
      end
})
local cpu_arr = wibox.container.background(wibox.container.margin(wibox.widget { cpuicon, cpu.widget, layout = wibox.layout.align.horizontal }, 3, 4), "#4B696D")
cpu_arr:connect_signal(
   "button::press",
   function()
      awful.spawn.easy_async(
         "ps -a -u " .. os.getenv("USER") .. " -o comm,pid,%cpu,%mem --sort=-%cpu",
         function(stdout, stderr, reason, exit_code)
            infobox:show_text(stdout, {bg="#4B696D"})
         end
      )
end)

--[[ Coretemp (lm_sensors, per core)
   local tempwidget = awful.widget.watch({awful.util.shell, '-c', 'sensors | grep Core'}, 30,
   function(widget, stdout)
   local temps = ""
   for line in stdout:gmatch("[^\r\n]+") do
   temps = temps .. line:match("+(%d+).*°C")  .. "° " -- in Celsius
   end
   widget:set_markup(markup.font(theme.font, " " .. temps))
   end)
--]]

--Coretemp (lain, average)
local temp = lain.widget.temp({
      settings = function()
         if type(coretemp_now) == "number" then
            coretemp_now = math.ceil(coretemp_now)
         end
         widget:set_markup(markup.font(theme.font, " " .. coretemp_now .. "°C "))
      end
})

local tempicon = wibox.widget.imagebox(theme.widget_temp)
local temp_arr = wibox.container.background(wibox.container.margin(wibox.widget { tempicon, temp.widget, layout = wibox.layout.align.horizontal }, 4, 4), "#4B3B51")
temp_arr:connect_signal(
   "button::press",
   function()
      awful.spawn.easy_async(
         "sensors",
         function(stdout, stderr, reason, exit_code)
            infobox:show_text(stdout, {bg="#4B3B51"})
         end
      )
end)

-- / fs
-- local fsicon = wibox.widget.imagebox(theme.widget_hdd)
-- theme.fs = lain.widget.fs({
--     options  = "--exclude-type=tmpfs",
--     notification_preset = { fg = theme.fg_normal, bg = theme.bg_normal, font = "xos4 Terminus 10" },
--     settings = function()
--         widget:set_markup(markup.font(theme.font, " " .. fs_now.available_gb .. "GB "))
--     end
--})

-- Battery
local bat_icon = wibox.widget.imagebox(theme.widget_battery)
local bat = lain.widget.bat({
      settings = function()
         if bat_now.ac_status ~= "N/A" then
            if bat_now.ac_status == 1 and tonumber(bat_now.perc) == 100 then
               widget:set_markup(markup.font(theme.font, " AC "))
               bat_icon:set_image(theme.widget_ac)
               return
            elseif bat_now.ac_status == 1 and tonumber(bat_now.perc) < 100 then
               widget:set_markup(markup.font(theme.font, " " .. bat_now.perc .. "% "))
               bat_icon:set_image(theme.widget_ac)
               return
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 5 then
               bat_icon:set_image(theme.widget_battery_empty)
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 15 then
               bat_icon:set_image(theme.widget_battery_low)
            else
               bat_icon:set_image(theme.widget_battery)
            end
            widget:set_markup(markup.font(theme.font, " " .. bat_now.perc .. "% "))
         else
            widget:set_markup()
            bat_icon:set_image(theme.widget_ac)
         end
      end
})

-- Net
local neticon = wibox.widget.imagebox(theme.widget_net)
local net = lain.widget.net({
      settings = function()
         widget:set_markup(markup.fontfg(theme.font, "#FEFEFE", " " .. net_now.received .. " ↓↑ " .. net_now.sent .. " "))
      end
})

-- Weather
--theme.weather = lain.widget.weather({
--    city_id = 524901
--})

-- Separators
local arrow = separators.arrow_left

function theme.powerline_rl(cr, width, height)
   local arrow_depth, offset = height/2, 0

   -- Avoid going out of the (potential) clip area
   if arrow_depth < 0 then
      width  =  width + 2*arrow_depth
      offset = -arrow_depth
   end

   cr:move_to(offset + arrow_depth         , 0        )
   cr:line_to(offset + width               , 0        )
   cr:line_to(offset + width - arrow_depth , height/2 )
   cr:line_to(offset + width               , height   )
   cr:line_to(offset + arrow_depth         , height   )
   cr:line_to(offset                       , height/2 )

   cr:close_path()
end

local function pl(widget, bgcolor, padding)
   return wibox.container.background(wibox.container.margin(widget, 16, 16), bgcolor, theme.powerline_rl)
end

function theme.at_screen_connect(s)
   -- Quake application
   s.quake = lain.util.quake({ app = awful.util.terminal })

   -- If wallpaper is a function, call it with the screen
   local wallpaper = theme.wallpaper
   if type(wallpaper) == "function" then
      wallpaper = wallpaper(s)
   end
   gears.wallpaper.maximized(wallpaper, s, true)

   -- Tags
   awful.tag(awful.util.tagnames, s, awful.layout.suit.tile)

   -- Create a promptbox for each screen
   s.mypromptbox = awful.widget.prompt()
   -- Create an imagebox widget which will contains an icon indicating which layout we're using.
   -- We need one layoutbox per screen.
   s.mylayoutbox = awful.widget.layoutbox(s)
   s.mylayoutbox:buttons(awful.util.table.join(
                            awful.button({ }, 1, function () awful.layout.inc( 1) end),
                            awful.button({ }, 3, function () awful.layout.inc(-1) end),
                            awful.button({ }, 4, function () awful.layout.inc( 1) end),
                            awful.button({ }, 5, function () awful.layout.inc(-1) end)))
   -- Create a taglist widget
   s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, awful.util.taglist_buttons)

   -- Create a tasklist widget
   s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, awful.util.tasklist_buttons)

   -- Create the wibox
   s.mywibox = awful.wibar({ position = "top", screen = s, height = 16, bg = theme.bg_normal, fg = theme.fg_normal })

   -- Add widgets to the wibox
   s.mywibox:setup {
      layout = wibox.layout.align.horizontal,
      { -- Left widgets
         layout = wibox.layout.fixed.horizontal,
         --spr,
         s.mytaglist,
         s.mypromptbox,
         spr,
      },
      s.mytasklist, -- Middle widget
      { -- Right widgets
         layout = wibox.layout.fixed.horizontal,
         theme.tray,
         --wibox.container.margin(scissors, 4, 8),
         --[[ using shapes
            pl(wibox.widget { mpdicon, theme.mpd.widget, layout = wibox.layout.align.horizontal }, "#343434"),
            pl(task, "#343434"),
            --pl(wibox.widget { mailicon, mail and mail.widget, layout = wibox.layout.align.horizontal }, "#343434"),
            pl(wibox.widget { memicon, mem.widget, layout = wibox.layout.align.horizontal }, "#777E76"),
            pl(wibox.widget { cpuicon, cpu.widget, layout = wibox.layout.align.horizontal }, "#4B696D"),
            pl(wibox.widget { tempicon, temp.widget, layout = wibox.layout.align.horizontal }, "#4B3B51"),
            pl(wibox.widget { fsicon, theme.fs.widget, layout = wibox.layout.align.horizontal }, "#CB755B"),
            pl(wibox.widget { baticon, bat.widget, layout = wibox.layout.align.horizontal }, "#8DAA9A"),
            pl(wibox.widget { neticon, net.widget, layout = wibox.layout.align.horizontal }, "#C0C0A2"),
            pl(binclock.widget, "#777E76"),
         --]]
         -- using separators
         arrow(theme.bg_focus, "#222A31"),
         wibox.container.background(wibox.container.margin(wibox.widget { mpdicon, theme.mpd.widget, layout = wibox.layout.align.horizontal }, 3, 6), "#222A31"),
         --arrow(theme.bg_normal, "#343434"),
         --wibox.container.background(wibox.container.margin(task, 3, 7), "#343434"),
         arrow("#222A31", "#3f4251"),
         pkg_arr,
         arrow("#3f4251", "#343454"),
         wibox.container.background(wibox.container.margin(wibox.widget { mailicon, mail and mail.widget, layout = wibox.layout.align.horizontal }, 4, 7), "#343454"),
         arrow("#343454", "#777E76"),
         mem_arr,
         arrow("#777E76", "#4B696D"),
         cpu_arr,
         arrow("#4B696D", "#4B3B51"),
         temp_arr,
         arrow("#4B3B51", "#CB755B"),
         wibox.container.background(wibox.container.margin(wibox.widget { volicon, theme.volume.widget, layout = wibox.layout.align.horizontal }, 3, 3), "#CB755B"),
         arrow("#CB755B", "#8DAA9A"),
         wibox.container.background(wibox.container.margin(wibox.widget { bat_icon, bat.widget, layout = wibox.layout.align.horizontal }, 3, 3), "#8DAA9A"),
         arrow("#8DAA9A", "#C0C0A2"),
         wibox.container.background(wibox.container.margin(wibox.widget { neticon, net.widget, layout = wibox.layout.align.horizontal }, 3, 3), "#C0C0A2"),
         arrow("#C0C0A2", "#777E76"),
         wibox.container.background(wibox.container.margin(textclock, 3, 3), "#777E76"),
         arrow("#777E76", "#222A31"),
         wibox.container.background(wibox.container.margin(keyboardlayout.widget, 3, 3), "#222A31"),
         arrow("#222A31", "alpha"),
         s.mylayoutbox,
      },
   }
end


return theme
