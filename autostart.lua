local autostartApps = {
   "numlockx on",
   "/usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1",
   "light-locker --no-late-locking --lock-after-screensaver=20",
   "spacefm -d",
   "telegram-desktop -startintray",
   "gnome-ring -r",
   "redshift-gtk",
   "nextcloud",
   "qbittorrent",
}

return autostartApps
