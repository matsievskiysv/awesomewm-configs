-- {{{ Required libraries
local awesome, client, mouse, screen, tag = awesome, client, mouse, screen, tag
local ipairs, string, os, table, tostring, tonumber, type = ipairs, string, os, table, tostring, tonumber, type

local gears         = require("gears")
local awful         = require("awful")
require("awful.autofocus")
require("awful.remote")
local wibox         = require("wibox")
local beautiful     = require("beautiful")
local naughty       = require("naughty")
local lain          = require("lain")
local menubar       = require("menubar")
local freedesktop   = require("freedesktop")
local hotkeys_popup = require("awful.hotkeys_popup").widget
local keybinds      = require("keybinds")
local modalbind     = require("modalbind")
local autostartApps = require("autostart")
local keyaliases    = require("keyaliases")
-- }}}

-- {{{ Error handling
if awesome.startup_errors then
   naughty.notify({ preset = naughty.config.presets.critical,
                    title = "Oops, there were errors during startup!",
                    text = awesome.startup_errors })
end

do
   local in_error = false
   awesome.connect_signal("debug::error", function (err)
                             if in_error then return end
                             in_error = true

                             naughty.notify({ preset = naughty.config.presets.critical,
                                              title = "Oops, an error happened!",
                                              text = tostring(err) })
                             in_error = false
   end)
end
-- }}}

-- {{{ Variable definitions
local chosen_theme     = "powerarrow"
local modkey           = "Mod4"
local altkey           = "Mod1"
local terminal         = "stterm -f FiraCode:pixelsize=20:lcdfilter=lcddefault:hintstyle=hintnone:rgba=rgb:antialias=true:autohint=false"
local multterminal     = "stterm -f FiraCode:pixelsize=20:lcdfilter=lcddefault:hintstyle=hintnone:rgba=rgb:antialias=true:autohint=false dvtm -title stterm -M -h 10000"
local editor           = os.getenv("EDITOR") or "emacs -nw"


awful.util.terminal = terminal
awful.util.tagnames = { "1", "2", "3", "4", "5" }
awful.layout.layouts = {
   awful.layout.suit.tile,
   awful.layout.suit.tile.left,
   awful.layout.suit.tile.bottom,
   awful.layout.suit.tile.top,
   -- awful.layout.suit.floating,
   -- awful.layout.suit.fair,
   -- awful.layout.suit.fair.horizontal,
   -- awful.layout.suit.spiral,
   -- awful.layout.suit.spiral.dwindle,
   -- awful.layout.suit.max,
   -- awful.layout.suit.max.fullscreen,
   -- awful.layout.suit.magnifier,
   -- awful.layout.suit.corner.nw,
   -- awful.layout.suit.corner.ne,
   -- awful.layout.suit.corner.sw,
   -- awful.layout.suit.corner.se,
   -- lain.layout.cascade,
   -- lain.layout.cascade.tile,
   -- lain.layout.centerwork,
   -- lain.layout.centerwork.horizontal,
   -- lain.layout.termfair,
   -- lain.layout.termfair.center,
}
awful.util.taglist_buttons = awful.util.table.join(
   awful.button({ }, 1, function(t) t:view_only() end),
   awful.button({ modkey }, 1, function(t)
         if client.focus then
            client.focus:move_to_tag(t)
         end
   end),
   awful.button({ }, 3, awful.tag.viewtoggle),
   awful.button({ modkey }, 3, function(t)
         if client.focus then
            client.focus:toggle_tag(t)
         end
   end),
   awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
   awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
)
awful.util.tasklist_buttons = awful.util.table.join(
   awful.button({ }, 1, function (c)
         if c == client.focus then
            c.minimized = true
         else
            -- Without this, the following
            -- :isvisible() makes no sense
            c.minimized = false
            if not c:isvisible() and c.first_tag then
               c.first_tag:view_only()
            end
            -- This will also un-minimize
            -- the client, if needed
            client.focus = c
            c:raise()
         end
   end),
   awful.button({ }, 3, function()
         local instance = nil

         return function ()
            if instance and instance.wibox.visible then
               instance:hide()
               instance = nil
            else
               instance = awful.menu.clients({ theme = { width = 250 } })
            end
         end
   end),
   awful.button({ }, 4, function ()
         awful.client.focus.byidx(1)
   end),
   awful.button({ }, 5, function ()
         awful.client.focus.byidx(-1)
end))

lain.layout.termfair.nmaster           = 3
lain.layout.termfair.ncol              = 1
lain.layout.termfair.center.nmaster    = 3
lain.layout.termfair.center.ncol       = 1
lain.layout.cascade.tile.offset_x      = 2
lain.layout.cascade.tile.offset_y      = 32
lain.layout.cascade.tile.extra_padding = 5
lain.layout.cascade.tile.nmaster       = 5
lain.layout.cascade.tile.ncol          = 2

local theme_path = string.format("%s/.config/awesome/themes/%s/theme.lua", os.getenv("HOME"), chosen_theme)
beautiful.init(theme_path)
-- }}}

-- {{{ Menu
local myawesomemenu = {
   { "upgrade system", function()
        awful.spawn(terminal .. " pkexec su -c 'apt update && apt upgrade -y && apt autoremove -y'")
   end },
   { "update menu", function()
	awful.spawn.easy_async("xdg-desktop-menu forceupdate",
			       function(stdout, stderr, exitreason, exitcode) end)
	menubar.refresh()
   end },
   { "hotkeys", function() return false, hotkeys_popup.show_help end },
   { "manual", terminal .. " -e man awesome" },
   { "edit config", string.format("%s -e %s %s", terminal, editor, awesome.conffile) },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end }
}

local powermenu = {
   { "shutdown", function() awful.spawn.easy_async("systemctl poweroff", function(stdout, stderr, exitreason, exitcode) end) end },
   { "reboot", function() awful.spawn.easy_async("systemctl reboot", function(stdout, stderr, exitreason, exitcode) end) end },
   { "lock session", function() awful.spawn.easy_async("dm-tool lock", function(stdout, stderr, exitreason, exitcode) end) end },
   { "slock session", function() awful.spawn.easy_async("slock", function(stdout, stderr, exitreason, exitcode) end) end },
   { "switch user",  function() awful.spawn.easy_async("dm-tool switch-to-greeter", function(stdout, stderr, exitreason, exitcode) end) end }
}


awful.util.mymainmenu = freedesktop.menu.build({
      icon_size = beautiful.menu_height or 16,
      before = {
         { "Awesome", myawesomemenu, beautiful.awesome_icon },
         -- other triads can be put here
      },
      after = {
         { "Open terminal", terminal, beautiful.terminal },
         { "Power menu", powermenu, beautiful.awesome_icon },
         -- other triads can be put here
      }
})
menubar.utils.terminal = terminal -- Set the Menubar terminal for applications that require it
-- }}}

-- {{{ Screen
-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", function(s)
                         -- Wallpaper
                         if beautiful.wallpaper then
                            local wallpaper = beautiful.wallpaper
                            -- If wallpaper is a function, call it with the screen
                            if type(wallpaper) == "function" then
                               wallpaper = wallpaper(s)
                            end
                            gears.wallpaper.maximized(wallpaper, s, true)
                         end
end)
-- Create a wibox for each screen and add it
awful.screen.connect_for_each_screen(function(s) beautiful.at_screen_connect(s) end)
-- }}}

-- {{{ Random wallpapers
function scandir(directory, filter)
   local i, t, popen = 0, {}, io.popen
   if not filter then
      filter = function(s) return true end
   end

   for filename in popen('ls -a "'..directory..'"'):lines() do
      if filter(filename) then
         i = i + 1
         t[i] = filename
      end
   end
   return t
end

current_wallpaper = {}
do
   local wp_path = string.format("%s/.config/awesome/wallpapers/", os.getenv("HOME")) -- is a symlink to wallpaper folder
   local wp_filter = function(s) return string.match(s,"%.png$") or string.match(s,"%.jpg$") or string.match(s,"%.jpeg$") end
   local wp_files = scandir(wp_path, wp_filter)

   math.randomseed( os.time() )
   local wallpapers = gears.timer {
      timeout = 300,
      autostart = true,
      callback = function()
         for s = 1, screen.count() do
            current_wallpaper[s] = wp_path .. wp_files[math.random(1, #wp_files)]
            gears.wallpaper.fit(current_wallpaper[s], s)
         end
      end
   }
end
-- }}}

-- {{{ ModalBind
modalbind.init()
modalbind.set_location("top_left")
modalbind.set_x_offset(5)
modalbind.set_y_offset(5)
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
                awful.button({ }, 3, function () awful.util.mymainmenu:toggle() end),
                awful.button({ }, 4, awful.tag.viewnext),
                awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
   -- Take a screenshot
   -- https://github.com/copycat-killer/dots/blob/master/bin/screenshot
   -- awful.key({ altkey }, "p", function() os.execute("screenshot") end),

   -- Hotkeys
   awful.key({ modkey,           }, "i",      hotkeys_popup.show_help,
      {description="show help", group="awesome"}),
   -- Tag browsing
   awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
      {description = "view previous", group = "tag"}),
   awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
      {description = "view next", group = "tag"}),
   awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
      {description = "go back", group = "tag"}),

   -- By direction client focus
   awful.key({ modkey }, "l",
      function()
         awful.client.focus.bydirection("down")
         if client.focus then client.focus:raise() end
   end),
   awful.key({ modkey }, "k",
      function()
         awful.client.focus.bydirection("up")
         if client.focus then client.focus:raise() end
   end),
   awful.key({ modkey }, "j",
      function()
         awful.client.focus.bydirection("left")
         if client.focus then client.focus:raise() end
   end),
   awful.key({ modkey }, ";",
      function()
         awful.client.focus.bydirection("right")
         if client.focus then client.focus:raise() end
   end),

   awful.key({ modkey,           }, "w", function () awful.util.mymainmenu:show() end,
      {description = "show main menu", group = "awesome"}),

   -- Layout manipulation
   awful.key({ modkey, altkey    }, ";", function () awful.client.swap.bydirection("right") end,
      {description = "swap with next client to the right", group = "client"}),
   awful.key({ modkey, altkey    }, "j", function () awful.client.swap.bydirection("left") end,
      {description = "swap with next client to the left", group = "client"}),
   awful.key({ modkey, altkey    }, "k", function () awful.client.swap.bydirection("up") end,
      {description = "swap with upper client", group = "client"}),
   awful.key({ modkey, altkey    }, "l", function () awful.client.swap.bydirection("down") end,
      {description = "swap with lower client", group = "client"}),

   -- Move tags
   awful.key({ modkey, altkey }, "Right", function () lain.util.move_tag(1) end,
      {description = "move tag right", group = "tag"}),
   awful.key({ modkey, altkey }, "Left", function () lain.util.move_tag(-1) end,
      {description = "move tag left", group = "tag"}),

   -- Screen focus
   awful.key({ modkey, "Control" }, ";",
      function()
         awful.screen.focus_bydirection("right", awful.screen.focused())
      end,
      {description = "focus the right screen", group = "screen"}),
   awful.key({ modkey, "Control" }, "j",
      function()
         awful.screen.focus_bydirection("left", awful.screen.focused())
      end,
      {description = "focus the left screen", group = "screen"}),
   awful.key({ modkey, "Control" }, "k",
      function()
         awful.screen.focus_bydirection("up", awful.screen.focused())
      end,
      {description = "focus the top screen", group = "screen"}),
   awful.key({ modkey, "Control" }, "l",
      function()
         awful.screen.focus_bydirection("down", awful.screen.focused())
      end,
      {description = "focus the bottom screen", group = "screen"}),

   awful.key({ modkey,           }, "Tab",
      function ()
         awful.client.focus.history.previous()
         if client.focus then
            client.focus:raise()
         end
      end,
      {description = "go back", group = "client"}),

   awful.key({ modkey }, "Home", function() beautiful.tray:set_screen(awful.screen.focused()) end),

   awful.key({ modkey, altkey    }, "Up", function() keybinds.pa_with_default_sink("pactl set-sink-volume %s +5%%") end,
      {description = "volume up", group = "media"}),
   awful.key({ }, "XF86AudioRaiseVolume", function() keybinds.pa_with_default_sink("pactl set-sink-volume %s +1%%") end,
      {description = "volume up", group = "media"}),
   awful.key({ modkey, altkey    }, "Down", function() keybinds.pa_with_default_sink("pactl set-sink-volume %s -5%%") end,
      {description = "volume down", group = "media"}),
   awful.key({ }, "XF86AudioLowerVolume", function() keybinds.pa_with_default_sink("pactl set-sink-volume %s -1%%") end,
      {description = "volume up", group = "media"}),
   awful.key({ }, "XF86AudioMute", function() keybinds.pa_with_default_sink("pactl set-sink-mute %s toggle") end,
      {description = "mute", group = "media"}),
   awful.key({ }, "XF86AudioPrev",
      function()
         awful.util.spawn("mpc prev -h " .. beautiful.mpd.host .. " -p " .. beautiful.mpd.port)
         beautiful.mpd.update()
      end,
      {description = "previous", group = "media"}),
   awful.key({ }, "XF86AudioNext",
      function()
         awful.util.spawn("mpc next -h " .. beautiful.mpd.host .. " -p " .. beautiful.mpd.port)
         beautiful.mpd.update()
      end,
      {description = "next", group = "media"}),
   awful.key({ }, "XF86AudioPlay",
      function()
         awful.util.spawn("mpc toggle -h " .. beautiful.mpd.host .. " -p " .. beautiful.mpd.port)
         beautiful.mpd.update()
      end,
      {description = "play", group = "media"}),

   -- Standard program
   awful.key({ modkey,           }, "Return", function () awful.spawn(multterminal) end,
      {description = "open a terminal", group = "launcher"}),

   -- Media control
   awful.key({ modkey }, "m", function() modalbind.grab{keymap = keybinds.mediamap,
                                                        name = "Media",
                                                        stay_in_mode = true,
                                                        layout = 0,
                                                        case_insensitive = true} end,
      {description = "media", group = "awesome"}),

   -- User programs
   awful.key({ modkey }, "x", function() modalbind.grab{keymap = keybinds.prgmap,
                                                        name = "Run",
                                                        stay_in_mode = false,
                                                        layout = 0,
                                                        case_insensitive = true} end,
      {description = "programs", group = "launcher"}),

   -- User shortcuts
   awful.key({ modkey }, "z", function() modalbind.grab{keymap = keybinds.shortmap,
                                                        name = "Shortcut",
                                                        stay_in_mode = false,
                                                        layout = 0,
                                                        case_insensitive = true} end,
      {description = "shortcuts", group = "launcher"}),

   -- Tags
   awful.key({ modkey }, "t", function() modalbind.grab{keymap = keybinds.tagsmap,
                                                        name = "Tags",
                                                        stay_in_mode = false,
                                                        layout = 0,
                                                        case_insensitive = true} end,
      {description = "tags", group = "tag"}),

   -- Window options
   awful.key({ modkey }, "s", function() modalbind.grab{keymap = keybinds.windowmap,
                                                        name = "Window",
                                                        stay_in_mode = true,
                                                        layout = 0,
                                                        case_insensitive = true} end,
      {description = "window", group = "layout"}),

   -- Awesome options
   awful.key({ modkey }, "a", function() modalbind.grab{keymap = keybinds.awesomemap,
                                                        name = "Awesome",
                                                        stay_in_mode = false,
                                                        layout = 0,
                                                        case_insensitive = true} end,
      {description = "awesome", group = "awesome"}),

   -- Default
   -- Menubar
   awful.key({ modkey }, "p", function() menubar.show() end,
      {description = "show the menubar", group = "launcher"}),

   -- Prompt
   awful.key({ modkey }, "r", function () awful.screen.focused().mypromptbox:run() end, {
         description = "run prompt",
         group = "launcher",
         history_path = awful.util.get_cache_dir() .. "/history_prompt"
   })
)

clientkeys = awful.util.table.join(
   awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill() end,
      {description = "close", group = "client"}),
   -- Client options
   awful.key({ modkey }, "c", function(c) modalbind.grab{keymap = keybinds.clientmap,
                                                         name = "Client",
                                                         stay_in_mode = false,
                                                         args = c,
                                                         layout = 0,
                                                         case_insensitive = true} end,
      {description = "Menu", group = "client"}),
   -- Move client
   awful.key({ modkey, "Shift"    }, "j", function (c) c:move_to_screen(c.screen:get_next_in_direction("left") or c.screen) end,
      {description = "move to left screen", group = "client"}),
   awful.key({ modkey, "Shift"    }, ";", function (c) c:move_to_screen(c.screen:get_next_in_direction("right") or c.screen) end,
      {description = "move to right screen", group = "client"}),
   awful.key({ modkey, "Shift"    }, "k", function (c) c:move_to_screen(c.screen:get_next_in_direction("up") or c.screen) end,
      {description = "move to top screen", group = "client"}),
   awful.key({ modkey, "Shift"    }, "l", function (c) c:move_to_screen(c.screen:get_next_in_direction("down") or c.screen) end,
      {description = "move to bottom screen", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
   globalkeys = awful.util.table.join(
      globalkeys,
      -- View tag only.
      awful.key({ modkey }, "#" .. i + 9,
         function ()
            local screen = awful.screen.focused()
            local tag = screen.tags[i]
            if tag then
               tag:view_only()
            end
         end,
         {description = "view tag #"..i, group = "tag"}),
      -- Toggle tag display.
      awful.key({ modkey, "Control" }, "#" .. i + 9,
         function ()
            local screen = awful.screen.focused()
            local tag = screen.tags[i]
            if tag then
               awful.tag.viewtoggle(tag)
            end
         end,
         {description = "toggle tag #" .. i, group = "tag"}),
      -- Move client to tag.
      awful.key({ modkey, "Shift" }, "#" .. i + 9,
         function ()
            if client.focus then
               local tag = client.focus.screen.tags[i]
               if tag then
                  client.focus:move_to_tag(tag)
               end
            end
         end,
         {description = "move focused client to tag #"..i, group = "tag"}),
      -- Toggle tag on focused client.
      awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
         function ()
            if client.focus then
               local tag = client.focus.screen.tags[i]
               if tag then
                  client.focus:toggle_tag(tag)
               end
            end
         end,
         {description = "toggle focused client on tag #" .. i, group = "tag"})
   )
end

clientbuttons = awful.util.table.join(
   awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
   awful.button({ modkey }, 1, awful.mouse.client.move),
   awful.button({ modkey }, 3, awful.mouse.client.resize))

-- {{{ Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
   -- All clients will match this rule.
   { rule = { },
     properties = { border_width = beautiful.border_width,
                    border_color = beautiful.border_normal,
                    focus = awful.client.focus.filter,
                    raise = true,
                    keys = clientkeys,
                    buttons = clientbuttons,
                    screen = awful.screen.preferred,
                    placement = awful.placement.no_overlap+awful.placement.no_offscreen,
                    size_hints_honor = false
     }
   },

   -- Titlebars
   { rule_any = { type = { "dialog", "normal" } },
     properties = { titlebars_enabled = false } },

   -- Set Firefox to always map on the first tag on screen 1.
      -- { rule = { class = "Firefox" },
      --   properties = { screen = 1, tag = screen[1].tags[1] } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
                         -- Set the windows at the slave,
                         -- i.e. put it at the end of others instead of setting it master.
                         -- if not awesome.startup then awful.client.setslave(c) end

                         if awesome.startup and
                            not c.size_hints.user_position
                         and not c.size_hints.program_position then
                            -- Prevent clients from being unreachable after screen count changes.
                            awful.placement.no_offscreen(c)
                         end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
                         -- Custom
                         if beautiful.titlebar_fun then
                            beautiful.titlebar_fun(c)
                            return
                         end

                         -- Default
                         -- buttons for the titlebar
                         local buttons = awful.util.table.join(
                            awful.button({ }, 1, function()
                                  client.focus = c
                                  c:raise()
                                  awful.mouse.client.move(c)
                            end),
                            awful.button({ }, 3, function()
                                  client.focus = c
                                  c:raise()
                                  awful.mouse.client.resize(c)
                            end)
                         )

                         awful.titlebar(c, {size = 16}) : setup {
                            { -- Left
                               awful.titlebar.widget.iconwidget(c),
                               buttons = buttons,
                               layout  = wibox.layout.fixed.horizontal
                            },
                            { -- Middle
                               { -- Title
                                  align  = "center",
                                  widget = awful.titlebar.widget.titlewidget(c)
                               },
                               buttons = buttons,
                               layout  = wibox.layout.flex.horizontal
                            },
                            { -- Right
                               awful.titlebar.widget.floatingbutton (c),
                               awful.titlebar.widget.maximizedbutton(c),
                               awful.titlebar.widget.stickybutton   (c),
                               awful.titlebar.widget.ontopbutton    (c),
                               awful.titlebar.widget.closebutton    (c),
                               layout = wibox.layout.fixed.horizontal()
                            },
                            layout = wibox.layout.align.horizontal
                                                                }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
                         if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
                         and awful.client.focus.filter(c) then
                            client.focus = c
                         end
end)

-- {{{ No border for maximized clients
client.connect_signal("focus",
                      function(c)
			 c.border_width = beautiful.border_width
			 c.border_color = beautiful.border_focus
end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

-- Disconnect the client ability to request different size and position
client.disconnect_signal("request::geometry", awful.ewmh.client_geometry_requests)

-- {{{ Connect action to widgets
-- beautiful.textclock:connect_signal("button::press",function()
--                                       awful.spawn.easy_async(calendar, function(stdout, stderr, exitreason, exitcode) end)
-- end)
-- }}}


-- {{{ Autorun programs
awesome.connect_signal(
   "startup",
   function()
      if true then -- autostart switch
         local lockfile = io.open("/tmp/.awesome_autostart_lock", "r")
         if lockfile==nil then
            for app = 1, #autostartApps do
               rc=awful.spawn(autostartApps[app])
               if type(rc) == string then
                  naughty.notify({preset = naughty.config.presets.critical,
                                  title = "Application startup error",
                                  text = string.format("%s\t%s",
                                                       autostartApps[app],
                                                       rc)})
               end
            end
            lockfile = io.open("/tmp/.awesome_autostart_lock", "w+")
         end
         lockfile:close()
      end
end)

awesome.connect_signal("exit", function(restart)
                          if restart == false then
                             os.remove("/tmp/.awesome_autostart_lock")
                          end
end)

-- }}}

require("tcprompt"){port=9876}

