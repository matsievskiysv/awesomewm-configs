# Installation steps #

```bash
git clone --recursive https://gitlab.com/matsievskiysv/awesomewm-configs ~/.config/awesome
```

# Debuging #

### Install `Xephyr`

```bash
sudo apt-get install xserver-xephyr
```


### Run `Xephyr`

```bash
DISPLAY=:0; Xephyr :1 -screen 800x600 -ac -br -noreset & \
DISPLAY=:1.0; sleep 1; awesome -c ~/.config/awesome/rc.lua
```
