local get_secret = require("get_secret")

local mailboxes = {}

mailboxes[1] = {
   server   = "imap.gmail.com",
   mail     = "mail1@gmail.com",
   password = function()
      return get_secret({lain_imap = 1})
   end
}

mailboxes[2] = {
   server   = "imap.gmail.com",
   mail     = "mail2@gmail.com",
   password = function()
      return get_secret({lain_imap = 2})
   end
}

mailboxes[3] = {
   server   = "imap.yandex.ru",
   mail     = "mail3@yandex.ru",
   password = function()
      return get_secret({lain_imap = 3})
   end
}

mailboxes[4] = {
   server   = "imap.yandex.ru",
   mail     = "mail4@yandex.ru",
   password = function()
      return get_secret({lain_imap = 4})
   end
}

return mailboxes
